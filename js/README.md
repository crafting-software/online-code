This is a new code-server environment to allow Javascript & Typescript development.

Pre-installed packages
- deno (https://deno.land/)
- volta (https://volta.sh/)
- node (from volta)
- npm (from volta)

Pre-installed extensions

- Code Runner (`formulahendry.code-runner`)
- Deno (`denoland.vscode-deno`)
- Docker (`ms-azuretools.vscode-docker`)

Built-in useful extensions

- Programming languages
  - `vscode.html`
  - `vscode.javascript`
  - `vscode.json`
  - `vscode.make`
  - `vscode.markdown`
  - `vscode.typescript`
  - `vscode.css`
- Features
  - `vscode.html-language-feature`
  - `ms-vscode.js-debug`
  - `vscode.json-language-features`
  - `vscode.debug-auto-launch`
  - `vscode.npm`
  - `vscode.simple-browser`
  - `vscode.typescript-language-features`

## Run javascript or typescript with deno

```bash
deno run file.js
deno run file.ts
```

## Run javascript with node

```
node file.js
```

Don't try to run Typescript with node unless you are ready to use npm, package.json ...
