IMAGE = registry.gitlab.com/crafting-software/online-code

base:
	docker build -t ${IMAGE}/base base

cpp: base
	docker build -t ${IMAGE}/c c

haskell: base
	docker build -t ${IMAGE}/haskell haskell

kotlin: base
	docker build -t ${IMAGE}/kotlin kotlin

python: base
	docker build -t ${IMAGE}/python python

racket: base
	docker build -t ${IMAGE}/racket racket

js: base
	docker build -t ${IMAGE}/js js

all: base cpp haskell kotlin python racket js


.PHONY: base cpp haskell kotlin python racket js
